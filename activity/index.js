const http = require ("http");
const port = 3000;

const server = http.createServer(function(request, response){
    if (request.url === "/login") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Welcome to login Page");
    }
    
    else{
        response.writeHead(404, {"Content-Type" : "text/plain"});
        response.end("Sorry the page you are looking for cannot be found");
    }
});
server.listen(port);

console.log("Your server is now running at port: 3000");