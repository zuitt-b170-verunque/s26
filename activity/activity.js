/* QUESTIONS */
// 1. What directive is used by Node.js in loading the modules it needs?
/* require */


// 2. What Node.js module contains a method for server creation?
/* createServer module */


// 3. What is the method of the http object responsible for creating a server using Node.js?
/* .createServer() */


// 4. What method of the response object allows us to set status codes and content types?
/* response.writeHead */	


// 5. Where will console.log() output its contents when run in Node.js?
/* at the terminal (gitbash) */


// 6. What property of the request object contains the address's endpoint?
/* response.end */





const http = require ("http");
const port = 3000;

const server = http.createServer(function(request, response){
    if (request.url === "/login") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Welcome to login Page");
    }
    
    else{
        response.writeHead(404, {"Content-Type" : "text/plain"});
        response.end("Sorry the page you are looking for cannot be found");
    }
});
server.listen(port);

console.log("Your server is now running at port: 3000");