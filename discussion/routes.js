const http = require ("http");
const port = 4000;


const server = http.createServer(function(request, response){
    if (request.url === "/greeting") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Hello World");
    }

    /* 
        Practice - create 2 more uri's and let your user see the message "Welcome to ________ Page"
    */
    else if (request.url === "/home") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Welcome to Home Page");
    }

    else if (request.url === "/about") {
        response.writeHead(200, {"Content-Type" : "text/plain"});
        response.end("Welcome to ABOUT Page");
    }
    // end of practice
    
    else{
        response.writeHead(404, {"Content-Type" : "text/plain"});
        response.end("Page not found");
    }
});

server.listen(port);

console.log(`Server now running at localhost ${port}`);