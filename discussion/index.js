let http = require("http");

http.createServer(function(request, response){
    response.writeHead(200,{"Content-Type":"text/plain"});
    response.end("Hello World")
}).listen(4000);

console.log("Your server is now running at port: 4000");